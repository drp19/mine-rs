#![warn(
    clippy::cast_possible_truncation,
    clippy::cast_possible_wrap,
    clippy::cast_sign_loss,
    clippy::useless_transmute
)]

#[cfg(feature = "logging")]
mod logging;
pub mod network;

#[cfg(all(test, feature = "logging"))]
#[test]
fn log() {
    use log::LevelFilter;

    logging::init(LevelFilter::Info);
}
mod test;
