use crate::network::models::{Context, ReadMCBytes, Size, WriteMCBytes};
use anyhow::Result;
use std::io::{Read, Write};

macro_rules! gen_prim_traits {
    ($a:ident) => {
        impl ReadMCBytes for $a {
            fn read_mc_bytes<R: Read>(
                bytes: &mut R,
                _context: &Context,
                _size: Option<usize>,
            ) -> Result<Self> {
                let mut buf = [0; ::std::mem::size_of::<$a>()];
                bytes.read_exact(&mut buf)?;
                Ok($a::from_be_bytes(buf))
            }
        }
        impl Size for $a {
            fn size(&self, _do_prefix_size: bool) -> usize {
                ::std::mem::size_of::<$a>()
            }
        }
        impl WriteMCBytes for $a {
            fn write_mc_bytes<W: Write>(&self, _no_size_prefix: bool, bytes: &mut W) -> Result<()> {
                bytes.write_all(&self.to_be_bytes())?;
                Ok(())
            }
        }
    };
}

gen_prim_traits!(u8);
gen_prim_traits!(i8);
gen_prim_traits!(u16);
gen_prim_traits!(i16);
gen_prim_traits!(i32);
gen_prim_traits!(i64);
gen_prim_traits!(f32);
gen_prim_traits!(f64);

/// BOOL
/// size: 1
/// to bytes: returns single byte of 0x1 or 0x0. Prefix size does nothing.
impl ReadMCBytes for bool {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        _context: &Context,
        _size: Option<usize>,
    ) -> Result<Self> {
        let mut buf = [0; 1];
        bytes.read_exact(&mut buf)?;
        Ok(buf[0] != 0)
    }
}

impl WriteMCBytes for bool {
    fn write_mc_bytes<W: Write>(&self, _no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        bytes.write_all(&[*self as u8])?;
        Ok(())
    }
}

impl Size for bool {
    fn size(&self, _do_prefix_size: bool) -> usize {
        1
    }
}
