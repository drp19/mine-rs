//! Contains wrappers and helper functions for all MC datatypes.
//!
//! | MC Type | Rust Type |
//! |-----------|-------------|
//! | Boolean | [`bool`]
//! | (Unsigned) Byte    | ([`u8`]) [`i8`]
//! | (Unsigned) Short   | ([`u16`]) [`i16`]
//! | Int    | [`i32`]
//! | Long    | [`i64`]
//! | Float    | [`f32`]
//! | Double    | [`f64`]
//! | String    | [`String`]
//! | Identifier | [`Identifier`]
//! | Chat | [`Chat`]
//! | VarInt | [`VarInt`]
//! | VarLong | [`VarLong`]
//!

use std::{
    convert::{TryFrom, TryInto},
    io::{Read, Write},
};

use anyhow::{anyhow, Result};

use mine_rs_derive::{ReadMCBytes, Size, WriteMCBytes};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::{Context, ReadMCBytes, Size, WriteMCBytes};

mod generics;
mod nbt;
mod prim;
mod varlen;

pub(crate) use generics::varint_enum;
pub(crate) use varlen::{VarInt, VarSize};

trait BaseDataType: WriteMCBytes + ReadMCBytes + Size {}

const MAX_STR_LEN: usize = 262144;

/// String
/// size: variable
/// to bytes: returns UTF-8 bytes. Prefix size on by default, appends byte len.
impl WriteMCBytes for String {
    #[allow(clippy::cast_possible_truncation)]
    fn write_mc_bytes<W: Write>(&self, no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        if self.len() > MAX_STR_LEN {
            return Err(anyhow!("String({}) is too long to serialize", self.len()));
        }
        if !no_size_prefix {
            VarInt::try_from(VarSize::new(self.len()))?.write_mc_bytes(false, bytes)?
        }
        bytes.write_all(self.as_bytes())?;
        Ok(())
    }
}

impl ReadMCBytes for String {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        context: &Context,
        size: Option<usize>,
    ) -> Result<Self> {
        let size = match size {
            Some(u) => u,
            None => VarSize::read_mc_bytes(bytes, context, None)?.get_inner() as usize,
        };
        let mut buf = vec![0; size];
        bytes.read_exact(&mut buf)?;
        let str = String::from_utf8(buf)?;
        Ok(str)
    }
}

impl Size for String {
    fn size(&self, do_prefix_size: bool) -> usize {
        if do_prefix_size {
            self.len() + VarSize::new(self.len()).size(false)
        } else {
            self.len()
        }
    }
}

impl WriteMCBytes for Uuid {
    fn write_mc_bytes<W: Write>(&self, _no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        bytes.write_all(self.as_bytes())?;
        Ok(())
    }
}
impl ReadMCBytes for Uuid {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        _context: &Context,
        _size: Option<usize>,
    ) -> Result<Self> {
        let mut buf = [0u8; 16];
        bytes.read_exact(&mut buf)?;
        Ok(Uuid::from_bytes(buf))
    }
}

impl Size for Uuid {
    fn size(&self, _do_prefix_size: bool) -> usize {
        16
    }
}

#[derive(WriteMCBytes, ReadMCBytes, Size, Debug)]
struct Slot {
    data: Option<SlotData>,
}

#[derive(WriteMCBytes, ReadMCBytes, Size, Debug)]
struct SlotData {
    item_id: VarInt,
    item_count: i8,
    nbt: Vec<u8>, //TODO
}

#[derive(ReadMCBytes, WriteMCBytes, Debug)]
#[encode_as(String)]
struct Identifier {
    namespace: String,
    val: String,
}

impl From<&Identifier> for String {
    fn from(value: &Identifier) -> Self {
        let mut str = value.namespace.clone();
        str.push(':');
        str.push_str(&value.val);
        str
    }
}
impl TryFrom<String> for Identifier {
    type Error = anyhow::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let colon_pos = value.find(':').ok_or(anyhow!("Need colon in ident"))?;
        Ok(Identifier {
            namespace: value[..colon_pos].to_string(),
            val: value[colon_pos + 1..].to_string(),
        })
    }
}

#[derive(ReadMCBytes, WriteMCBytes, Deserialize, Serialize, Debug)]
#[json]
pub struct Chat {
    text: String,
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use crate::network::models::{datatypes::ReadMCBytes, Context};

    use super::Slot;

    #[test]
    fn test_slot() {
        let not_present = vec![0];
        let mut not_present_cursor = Cursor::new(not_present);
        let slot = Slot::read_mc_bytes(&mut not_present_cursor, &Context::default(), None).unwrap();
        println!("{:?}", slot);
        let present = vec![1, 0xff, 0x01, 2, 0x02, 2, 3];
        let mut present_cursor = Cursor::new(present);
        let slot = Slot::read_mc_bytes(&mut present_cursor, &Context::default(), None).unwrap();
        println!("{:?}", slot);
    }
}
