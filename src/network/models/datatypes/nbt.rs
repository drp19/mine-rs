use crate::network::models::{Context, ReadMCBytes, Size, WriteMCBytes};
use anyhow::{anyhow, Result};
use mine_rs_derive::{ReadMCBytes, Size, WriteMCBytes};
use std::io::{Read, Write};

#[derive(ReadMCBytes, WriteMCBytes, Debug, PartialEq)]
#[id(size.map(|i| i as i32))]
#[prefix_with(|i| i as u8)]
enum NBT {
    #[id(0)]
    End,
    #[id(1)]
    Byte(NBTByte),
    #[id(2)]
    Short(NBTShort),
    #[id(3)]
    Int(NBTInt),
    #[id(4)]
    Long(NBTLong),
    #[id(5)]
    Float(NBTFloat),
    #[id(6)]
    Double(NBTDouble),
    #[id(7)]
    ByteArray(NBTByteArray),
    #[id(8)]
    String(NBTString),
    #[id(9)]
    List(NBTList),
    #[id(10)]
    Compound(NBTCompound),
    #[id(11)]
    IntArray(NBTIntArray),
    #[id(12)]
    LongArray(NBTLongArray),
    #[id(-1)]
    Unknown,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTByte {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    data: i8,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTShort {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    data: i16,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTInt {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    data: i32,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTLong {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    data: i64,
}
#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTFloat {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    data: f32,
}
#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTDouble {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    data: f64,
}
#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTByteArray {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    #[size(i32::read_mc_bytes(bytes, context, None)? as usize)]
    #[prefix_with(i32)]
    data: Vec<i8>,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTString {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    val: NBTStringInner,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTStringInner {
    #[size(u16::read_mc_bytes(bytes, context, None)? as usize)]
    #[prefix_with(u16)]
    val: String,
}

#[derive(Debug, PartialEq)]
struct NBTList {
    name: Option<NBTStringInner>,
    id: u8,
    data: Vec<NBT>,
}

#[derive(WriteMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTCompound {
    #[no_prefix]
    name: Option<NBTStringInner>,
    #[no_prefix]
    data: Vec<NBT>,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTIntArray {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    #[size(i32::read_mc_bytes(bytes, context, None)? as usize)]
    #[prefix_with(i32)]
    data: Vec<i32>,
}

#[derive(WriteMCBytes, ReadMCBytes, Debug, PartialEq)]
#[no_prefix]
struct NBTLongArray {
    #[size(if size.is_some() {0} else {1})]
    #[no_prefix]
    name: Option<NBTStringInner>,
    #[size(i32::read_mc_bytes(bytes, context, None)? as usize)]
    #[prefix_with(i32)]
    data: Vec<i64>,
}

impl WriteMCBytes for NBTList {
    fn write_mc_bytes<W: Write>(&self, _no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        self.name.write_mc_bytes(true, bytes)?;
        self.id.write_mc_bytes(false, bytes)?;
        (self.data.len() as i32).write_mc_bytes(false, bytes)?;
        for v in &self.data {
            v.write_mc_bytes(true, bytes)?;
        }
        Ok(())
    }
}

impl ReadMCBytes for NBTList {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        context: &Context,
        size: Option<usize>,
    ) -> Result<Self> {
        let name = Option::<NBTStringInner>::read_mc_bytes(
            bytes,
            context,
            Some(if size.is_some() { 0 } else { 1 }),
        )?;
        let id = u8::read_mc_bytes(bytes, context, None)?;
        let size = i32::read_mc_bytes(bytes, context, None)?;
        if size > 0 && id == 0 {
            return Err(anyhow!("Cannot have END tag with negative size."));
        }
        let mut data = Vec::new();
        for _ in 0..size {
            data.push(NBT::read_mc_bytes(bytes, context, Some(id as usize))?)
        }
        Ok(NBTList { name, id, data })
    }
}

impl ReadMCBytes for NBTCompound {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        context: &Context,
        size: Option<usize>,
    ) -> Result<Self> {
        let name = Option::<NBTStringInner>::read_mc_bytes(
            bytes,
            context,
            Some(if size.is_some() { 0 } else { 1 }),
        )?;
        let mut items = Vec::new();
        loop {
            let new_tag = NBT::read_mc_bytes(bytes, context, None)?;
            let end = matches!(new_tag, NBT::End);
            items.push(new_tag);
            if end {
                break;
            }
        }
        Ok(NBTCompound { name, data: items })
    }
}

#[cfg(test)]
mod test {
    use std::{
        fs::File,
        io::{BufReader, Cursor, Read},
    };

    use crate::network::models::{
        datatypes::nbt::{NBTByte, NBTStringInner},
        Context, ReadMCBytes, WriteMCBytes,
    };

    use super::NBT;

    #[test]
    fn short() {
        let bytes = &[0x01, 0x00, 0x02, 0x6e, 0x61, 0x05];
        let mut byte_cursor = Cursor::new(bytes);
        let nbt = NBT::read_mc_bytes(&mut byte_cursor, &Context::default(), None);
        println!("{:?}", nbt);
        assert_eq!(
            NBT::Byte(NBTByte {
                name: Some(NBTStringInner {
                    val: "na".to_string()
                }),
                data: 5
            }),
            nbt.unwrap()
        )
    }
    #[test]
    fn hello_world() {
        let bytes = &[
            0x0a, 0x00, 0x0b, 0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64,
            0x02, 0x00, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x10, 0x10, 0x00,
        ];
        let mut byte_cursor = Cursor::new(bytes);
        let nbt = NBT::read_mc_bytes(&mut byte_cursor, &Context::default(), None);
        println!("{:?}", nbt);
        let mut output = Vec::new();
        nbt.unwrap().write_mc_bytes(false, &mut output).unwrap();
        println!("{:02x?}", output);
    }

    #[test]
    fn list() {
        let bytes = &[
            0x09, 0x00, 0x01, 0x6e, 0x02, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x02, 0x00,
            0x03, 0x00, 0x04,
        ];
        let mut byte_cursor = Cursor::new(bytes);
        let nbt = NBT::read_mc_bytes(&mut byte_cursor, &Context::default(), None);
        println!("{:?}", nbt);
        let mut output = Vec::new();
        nbt.unwrap().write_mc_bytes(false, &mut output).unwrap();
        assert!(do_vecs_match(bytes, &output));
    }

    #[test]
    fn big() {
        let f = File::open("bigtest.nbt").unwrap();
        let mut reader = BufReader::new(f);
        let mut buffer = Vec::new();

        // Read file into vector.
        reader.read_to_end(&mut buffer).unwrap();

        let mut byte_cursor = Cursor::new(buffer.clone());
        let nbt = NBT::read_mc_bytes(&mut byte_cursor, &Context::default(), None);
        println!("{:?}", nbt);
        let mut output = Vec::new();
        nbt.unwrap().write_mc_bytes(false, &mut output).unwrap();
        assert!(do_vecs_match(&buffer, &output));
    }
    fn do_vecs_match<T: PartialEq>(a: &[T], b: &[T]) -> bool {
        let matching = a.iter().zip(b.iter()).filter(|&(a, b)| a == b).count();
        matching == a.len() && matching == b.len()
    }
}
