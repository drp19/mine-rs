use anyhow::Result;
use std::{
    convert::TryFrom,
    io::{Read, Write},
};

use crate::network::models::{Context, ReadMCBytes, Size, WriteMCBytes};

use super::{VarInt, VarSize};

impl<T: ReadMCBytes> ReadMCBytes for Vec<T> {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        context: &Context,
        size: Option<usize>,
    ) -> Result<Self> {
        let size = match size {
            Some(u) => u,
            None => VarSize::read_mc_bytes(bytes, context, None)?.get_inner() as usize,
        };
        let mut res = Vec::with_capacity(size);
        for _ in 0..size {
            res.push(T::read_mc_bytes(bytes, context, None)?);
        }
        Ok(res)
    }
}

impl<T: WriteMCBytes> WriteMCBytes for Vec<T> {
    fn write_mc_bytes<W: Write>(&self, no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        if !no_size_prefix {
            VarInt::try_from(VarSize::new(self.len()))?.write_mc_bytes(false, bytes)?
        }
        for i in self {
            i.write_mc_bytes(false, bytes)?;
        }
        Ok(())
    }
}

impl<T: WriteMCBytes> Size for Vec<T> {
    fn size(&self, do_prefix_size: bool) -> usize {
        if do_prefix_size {
            self.len() + VarSize::new(self.len()).size(false)
        } else {
            self.len()
        }
    }
}

impl<T: ReadMCBytes> ReadMCBytes for Option<T> {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        context: &Context,
        size: Option<usize>,
    ) -> Result<Self> {
        let present = match size {
            Some(u) => u != 0,
            None => bool::read_mc_bytes(bytes, context, None)?,
        };
        if present {
            Ok(Some(T::read_mc_bytes(bytes, context, None)?))
        } else {
            Ok(None)
        }
    }
}

impl<T: WriteMCBytes> WriteMCBytes for Option<T> {
    fn write_mc_bytes<W: Write>(&self, no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        if !no_size_prefix {
            self.is_some().write_mc_bytes(false, bytes)?;
        }
        if self.is_some() {
            self.as_ref().unwrap().write_mc_bytes(false, bytes)?;
        }
        Ok(())
    }
}

impl<T: WriteMCBytes> Size for Option<T> {
    fn size(&self, _do_prefix_size: bool) -> usize {
        if self.is_some() {
            1
        } else {
            0
        }
    }
}

macro_rules! varint_enum {
    ($(#[$meta:meta])* $vis:vis enum $name:ident {
        $($(#[$vmeta:meta])* $vname:ident $(= $val:expr)?,)*
    }) => {
        $(#[$meta])*
        $vis enum $name {
            $($(#[$vmeta])* $vname $(= $val)?,)*
        }

        impl std::convert::TryFrom<VarInt> for $name {
            type Error = anyhow::Error;

            fn try_from(v: VarInt) -> Result<Self, Self::Error> {
                match v.get_inner() {
                    $(x if x == $name::$vname as i32 => Ok($name::$vname),)*
                    z => Err(anyhow::anyhow!("{} is not an enum value for {}", z, "$name")),
                }
            }
        }

        impl std::convert::From< $name > for VarInt {
            fn from(s: $name) -> Self {
                VarInt::new(s as i32)
            }
        }
    }
}
pub(crate) use varint_enum;
