use std::{
    convert::TryFrom,
    io::{Read, Write},
};

use crate::network::{
    crypt::{Crypt, CryptReadStream},
    models::{Context, ReadMCBytes, Size, WriteMCBytes},
};
use anyhow::{anyhow, Result};
use tokio::{io::AsyncReadExt, net::tcp::OwnedReadHalf};

#[derive(Debug, Clone, Copy)]
pub(crate) struct VarInt {
    i: i32,
}

#[derive(Debug, Clone, Copy)]
pub(crate) struct VarSize {
    i: usize,
}

impl From<i32> for VarInt {
    fn from(i: i32) -> Self {
        Self { i }
    }
}

#[allow(clippy::cast_sign_loss)]
#[allow(clippy::cast_possible_truncation)]
impl WriteMCBytes for VarInt {
    fn write_mc_bytes<W: Write>(&self, _no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        let mut value = self.i as u32; //conversion needed to make shifting work later
        loop {
            if (value & !0x7F) == 0 {
                bytes.write_all(&[value as u8])?;
                break;
            }

            bytes.write_all(&[(value as u8 & 0x7F) | 0x80])?;
            value >>= 7;
        }
        Ok(())
    }
}

impl TryFrom<VarSize> for VarInt {
    type Error = anyhow::Error;

    #[allow(clippy::cast_possible_truncation)]
    #[allow(clippy::cast_possible_wrap)]
    fn try_from(value: VarSize) -> Result<Self, Self::Error> {
        if value.size(false) > 5 {
            Err(anyhow!("Cannot fit {} in VarInt", value.i))
        } else {
            Ok(VarInt { i: value.i as i32 })
        }
    }
}

impl ReadMCBytes for VarInt {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        _context: &Context,
        _size: Option<usize>,
    ) -> Result<Self> {
        let mut value = 0i32;
        let mut length = 0usize;
        let mut current_byte = [0u8; 1];
        loop {
            bytes.read_exact(&mut current_byte)?;
            value |= ((current_byte[0] & 0x7F) as i32) << (length * 7);

            length += 1;
            if length > 5 {
                return Err(anyhow!("Streamed Varint too large: {:?}", value));
            }

            if (current_byte[0] & 0x80) != 0x80 {
                break;
            }
        }
        Ok(VarInt { i: value })
    }
}

impl ReadMCBytes for VarSize {
    #[allow(clippy::cast_sign_loss)]
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        context: &Context,
        _size: Option<usize>,
    ) -> Result<Self> {
        let len = VarInt::read_mc_bytes(bytes, context, None)?;
        if len.get_inner() < 0 {
            return Err(anyhow!("Unsigned varlen negative: {:?}", len));
        }
        Ok(VarSize {
            i: len.i as u32 as usize,
        })
    }
}

impl Size for VarInt {
    #[allow(clippy::cast_sign_loss)]
    fn size(&self, _do_prefix_size: bool) -> usize {
        size(self.i as u32 as u64) //conversion needed to make shifting work later
    }
}

impl Size for VarSize {
    fn size(&self, _do_prefix_size: bool) -> usize {
        size(self.i as u64)
    }
}

fn size(mut value: u64) -> usize {
    let mut size = 1;
    loop {
        if (value & !0x7F) == 0 {
            return size;
        }
        size += 1;
        value >>= 7;
    }
}

impl VarInt {
    pub(crate) fn new(i: i32) -> Self {
        VarInt { i }
    }

    pub(crate) fn get_inner(&self) -> i32 {
        self.i
    }

    pub(crate) async fn from_stream(stream: &mut CryptReadStream<'_>) -> Result<Self> {
        let mut value = 0i32;
        let mut length = 0usize;
        let mut current_byte = [0u8; 1];
        loop {
            stream.read_exact(&mut current_byte).await?;
            value |= ((current_byte[0] & 0x7F) as i32) << (length * 7);

            length += 1;
            if length > 5 {
                return Err(anyhow!("Streamed Varint too large: {:?}", value));
            }

            if (current_byte[0] & 0x80) != 0x80 {
                break;
            }
        }
        Ok(VarInt { i: value })
    }
}

impl VarSize {
    pub(crate) fn new(i: usize) -> Self {
        VarSize { i }
    }

    pub(crate) fn get_inner(&self) -> usize {
        self.i
    }

    #[allow(clippy::cast_sign_loss)]
    pub(crate) async fn from_stream(stream: &mut CryptReadStream<'_>) -> Result<Self> {
        let len = VarInt::from_stream(stream).await?;
        if len.get_inner() < 0 {
            return Err(anyhow!("Unsigned varlen negative: {:?}", len));
        }
        Ok(VarSize {
            i: len.i as u32 as usize,
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub(crate) struct VarLong {
    pub(super) i: i64,
}

impl From<i64> for VarLong {
    fn from(i: i64) -> Self {
        Self { i }
    }
}

#[allow(clippy::cast_sign_loss)]
#[allow(clippy::cast_possible_truncation)]
impl WriteMCBytes for VarLong {
    fn write_mc_bytes<W: Write>(&self, _no_size_prefix: bool, bytes: &mut W) -> Result<()> {
        let mut value = self.i as u64; //conversion needed to make shifting work later
        loop {
            if (value & !0x7F) == 0 {
                bytes.write_all(&[value as u8])?;
                break;
            }

            bytes.write_all(&[(value as u8 & 0x7F) | 0x80])?;
            value >>= 7;
        }
        Ok(())
    }
}

impl ReadMCBytes for VarLong {
    fn read_mc_bytes<R: Read>(
        bytes: &mut R,
        _context: &Context,
        _size: Option<usize>,
    ) -> Result<Self> {
        let mut value = 0i64;
        let mut length = 0usize;
        let mut current_byte = [0u8; 1];
        loop {
            bytes.read_exact(&mut current_byte)?;
            value |= ((current_byte[0] & 0x7F) as i64) << (length * 7);

            length += 1;
            if length > 10 {
                return Err(anyhow!("Streamed Varlong too large: {:?}", value));
            }

            if (current_byte[0] & 0x80) != 0x80 {
                break;
            }
        }
        Ok(VarLong { i: value })
    }
}

impl Size for VarLong {
    #[allow(clippy::cast_sign_loss)]
    fn size(&self, _do_prefix_size: bool) -> usize {
        size(self.i as u64) //conversion needed to make shifting work later
    }
}

impl VarLong {
    pub(crate) fn new(i: i64) -> Self {
        VarLong { i }
    }

    pub(crate) fn get_inner(&self) -> i64 {
        self.i
    }

    pub(crate) async fn from_stream(
        stream: &mut OwnedReadHalf,
        encrypt: &mut Crypt,
    ) -> Result<Self> {
        let mut value = 0i64;
        let mut length = 0usize;
        let mut current_byte = [0u8; 1];
        loop {
            stream.read_exact(&mut current_byte).await?;
            encrypt.decrypt_data(&mut current_byte);
            value |= ((current_byte[0] & 0x7F) as i64) << (length * 7);

            length += 1;
            if length > 10 {
                return Err(anyhow!("Streamed Varlong too large: {:?}", value));
            }

            if (current_byte[0] & 0x80) != 0x80 {
                break;
            }
        }
        Ok(VarLong { i: value })
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use crate::network::models::{datatypes::VarInt, Context, ReadMCBytes, Size, WriteMCBytes};

    static TEST_NUMS: &[i32] = &[
        0,
        1,
        2,
        127,
        128,
        255,
        25565,
        2097151,
        2147483647,
        -1,
        -2147483648,
    ];
    static TEST_BYTES: &[&[u8]] = &[
        &[0],
        &[1],
        &[2],
        &[127],
        &[128, 1],
        &[255, 1],
        &[221, 199, 1],
        &[255, 255, 127],
        &[255, 255, 255, 255, 7],
        &[255, 255, 255, 255, 15],
        &[128, 128, 128, 128, 8],
    ];

    #[test]
    fn varint_write_bytes() {
        for i in TEST_NUMS.iter().zip(TEST_BYTES.iter()) {
            let mut output = Vec::new();
            VarInt { i: *i.0 }
                .write_mc_bytes(false, &mut output)
                .unwrap();
            assert!(do_vecs_match(&output, i.1));
        }
    }

    #[test]
    fn varint_size() {
        for i in TEST_NUMS.iter().zip(TEST_BYTES.iter()) {
            let mut input = Cursor::new(i.1);
            let res = VarInt::read_mc_bytes(&mut input, &Context::default(), None).unwrap();
            assert_eq!(res.size(false), i.1.len());
        }
    }

    #[test]
    fn varint_read_bytes() {
        for i in TEST_NUMS.iter().zip(TEST_BYTES.iter()) {
            let mut input = Cursor::new(i.1);
            let res = VarInt::read_mc_bytes(&mut input, &Context::default(), None).unwrap();
            assert_eq!(*i.0, res.i);
        }
    }

    fn do_vecs_match<T: PartialEq>(a: &[T], b: &[T]) -> bool {
        let matching = a.iter().zip(b.iter()).filter(|&(a, b)| a == b).count();
        matching == a.len() && matching == b.len()
    }
}
