use std::io::{Read, Write};

use anyhow::Result;

use self::datatypes::{varint_enum, VarInt, VarSize};

pub(crate) mod datatypes;
pub(crate) mod packet;

varint_enum! {
    #[derive(Debug, Clone, Copy, PartialEq)]
    #[allow(dead_code)]
    pub enum ConnectionStatus {
        Handshaking = 0,
        Status = 1,
        Login = 2,
        Play = 3,
    }
}

pub struct Context {
    pub(crate) status: ConnectionStatus,
}

impl Default for Context {
    fn default() -> Self {
        Self {
            status: ConnectionStatus::Handshaking,
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[allow(dead_code)]
pub(crate) enum Direction {
    Serverbound,
    Clientbound,
}

pub(crate) trait Size {
    fn size(&self, do_prefix_size: bool) -> usize;
}

pub(crate) trait WriteMCBytes {
    /// Convert the data into bytes.
    /// If `no_size_prefix` is set, datatypes that would normally
    /// add their size as a prefix will omit it.
    /// Setting it for other types, such as primatives, will do nothing.
    fn write_mc_bytes<W>(&self, no_size_prefix: bool, bytes: &mut W) -> Result<()>
    where
        W: Write,
        Self: Sized;
}

pub(crate) trait ReadMCBytes {
    /// Attempt to read bytes from the reader.
    /// If size is present, some types will use that size instead.
    /// Otherwise, the default size format is used.
    fn read_mc_bytes<R>(bytes: &mut R, context: &Context, size: Option<usize>) -> Result<Self>
    where
        Self: Sized,
        R: Read;
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{datatypes::VarInt, ConnectionStatus};

    #[test]
    fn test_status_enum() {
        let stat_var = VarInt::new(0);
        let stat = ConnectionStatus::try_from(stat_var);
        println!("{:?}", stat)
    }
}
