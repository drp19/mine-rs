use anyhow::{anyhow, Result};
use mine_rs_derive::{ReadMCBytes, Size, WriteMCBytes};
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::io::{Cursor, Read, Write};
use tokio::net::tcp::OwnedReadHalf;
use uuid::Uuid;

use crate::network::{
    compress::{Compress, CompressReadStream, CompressWriteStream},
    crypt::{Crypt, CryptReadStream, CryptWriteStream},
    models::Context,
};

use super::{datatypes::Chat, ConnectionStatus, ReadMCBytes, Size, VarInt, VarSize, WriteMCBytes};

const MAX_PACKET_LEN: usize = 2097151;

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[id(Some(VarInt::read_mc_bytes(bytes, context, None)?.get_inner() | (context.status as i32) << 8 | 0xFF0000))]
#[prefix_with(|i| (i & 0xff) as u8)]
pub enum DataPacket {
    #[id((ConnectionStatus::Status as i32) << 8 | 0xFF0001)]
    Response(ResponseData),
    #[id((ConnectionStatus::Login as i32) << 8 | 0xFF0002)]
    LoginSuccess(LoginSuccessData),
    #[id((ConnectionStatus::Login as i32) << 8 | 0xFF0003)]
    SetCompression(SetCompressionData),
    #[id((ConnectionStatus::Login as i32) << 8 | 0xFF0001)]
    EncryptionRequest(EncryptionRequestData),
    #[id((ConnectionStatus::Play as i32) << 8 | 0xFF001A)]
    Disconnect(DisconnectData),
    #[id((ConnectionStatus::Play as i32) << 8 | 0xFF0021)]
    KeepAlive(KeepAliveData),
    #[id((ConnectionStatus::Play as i32) << 8 | 0xFF0026)]
    JoinGame,
    #[id((ConnectionStatus::Handshaking as i32) << 8 | 0x00)]
    Handshake(HandshakeData),
    #[id((ConnectionStatus::Status as i32) << 8 | 0x00)]
    Request,
    #[id((ConnectionStatus::Login as i32) << 8 | 0x00)]
    LoginStart(LoginStartData),
    #[id((ConnectionStatus::Login as i32) << 8 | 0x01)]
    EncryptionResponse(EncryptionResponseData),
    #[id((ConnectionStatus::Play as i32) << 8 | 0x0F)]
    KeepAliveServer(KeepAliveData),
    #[id((ConnectionStatus::Play as i32) << 8 | 0x05)]
    ClientSettings(ClientSettingsData),
    #[id((ConnectionStatus::Play as i32) << 8 | 0x04)]
    ClientStatus(ClientStatusData),
    #[id(-1)]
    Unknown,
}

impl DataPacket {
    pub(crate) async fn from_stream(
        stream: &mut OwnedReadHalf,
        compression: &Compress,
        encrypt: &mut Crypt,
        context: &Context,
    ) -> anyhow::Result<Self> {
        let mut crypt_stream = CryptReadStream {
            crypt: encrypt,
            stream,
        };
        let size = VarSize::from_stream(&mut crypt_stream).await?;
        let mut comp_stream = CompressReadStream {
            compress: compression,
            stream: &mut crypt_stream,
            context,
        };
        let raw_data = comp_stream.read_bytes(size.get_inner() as usize).await?;
        let data_len = raw_data.len();
        let mut data_reader = Cursor::new(raw_data);
        DataPacket::read_mc_bytes(&mut data_reader, context, Some(data_len))
    }

    pub(crate) fn handshake(
        version: VarInt,
        address: String,
        port: u16,
        next_state: ConnectionStatus,
    ) -> Self {
        DataPacket::Handshake(HandshakeData {
            version,
            address,
            port,
            next_state,
        })
    }

    pub(crate) fn request() -> Self {
        DataPacket::Request
    }

    pub(crate) fn login_start(username: String) -> Self {
        DataPacket::LoginStart(LoginStartData { username })
    }

    pub(crate) fn encryption_response(secret: Vec<u8>, verify: Vec<u8>) -> Self {
        DataPacket::EncryptionResponse(EncryptionResponseData { secret, verify })
    }

    pub(crate) fn keep_alive(id: i64) -> Self {
        DataPacket::KeepAliveServer(KeepAliveData { id })
    }

    pub(crate) fn client_settings(
        locale: String,
        view_distance: i8,
        chat_mode: VarInt,
        chat_colors: bool,
        skin_parts: u8,
        hand: VarInt,
        text_filtering: bool,
        server_listings: bool,
    ) -> Self {
        DataPacket::ClientSettings(ClientSettingsData {
            locale,
            view_distance,
            chat_mode,
            chat_colors,
            skin_parts,
            hand,
            text_filtering,
            server_listings,
        })
    }

    pub(crate) fn client_status(action: VarInt) -> Self {
        DataPacket::ClientStatus(ClientStatusData { action })
    }

    pub(crate) fn to_bytes(&self, compression: &Compress, encrypt: &mut Crypt) -> Result<Vec<u8>> {
        let mut result = Vec::new();
        if self.size(false) > MAX_PACKET_LEN {
            return Err(anyhow!("Cannot encode packet of len {}", self.size(false)));
        }
        let mut crypt = CryptWriteStream {
            crypt: encrypt,
            stream: &mut result,
            buf: Vec::new(),
        };
        let mut comp = CompressWriteStream {
            compress: compression,
            stream: &mut crypt,
            buf: Vec::new(),
        };
        self.write_mc_bytes(false, &mut comp)?;
        comp.flush()?;
        Ok(result)
    }
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct SetCompressionData {
    pub(crate) threshold: VarInt,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct HandshakeData {
    version: VarInt,
    address: String,
    port: u16,
    #[encode_as(VarInt)]
    next_state: ConnectionStatus,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct EncryptionResponseData {
    secret: Vec<u8>,
    verify: Vec<u8>,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct ClientSettingsData {
    locale: String,
    view_distance: i8,
    chat_mode: VarInt,
    chat_colors: bool,
    skin_parts: u8,
    hand: VarInt,
    text_filtering: bool,
    server_listings: bool,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct ClientStatusData {
    pub(crate) action: VarInt,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct LoginStartData {
    username: String,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct EncryptionRequestData {
    pub(crate) server_id: String,
    pub(crate) key: Vec<u8>,
    pub(crate) verify_token: Vec<u8>,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct DisconnectData {
    pub(crate) reason: String,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct LoginSuccessData {
    uuid: Uuid,
    username: String,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
pub struct KeepAliveData {
    pub(crate) id: i64,
}
#[derive(ReadMCBytes, WriteMCBytes, Size, Debug)]
#[no_prefix]
#[require_size = "param"]
pub struct PluginMessage {
    pub(crate) channel: String,
    #[no_prefix]
    #[size(size.unwrap() - channel.size(true))]
    pub(crate) data: Vec<u8>,
}

#[derive(ReadMCBytes, WriteMCBytes, Size, Deserialize, Serialize, Debug)]
#[no_prefix]
#[json]
pub struct ResponseData {
    version: Version,
    players: Players,
    description: Chat,
    favicon: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Version {
    name: String,
    protocol: u32,
}

#[derive(Deserialize, Serialize, Debug)]
struct Players {
    max: u32,
    online: u32,
    sample: Option<Vec<PlayerSample>>,
}

#[derive(Deserialize, Serialize, Debug)]
struct PlayerSample {
    name: String,
    id: Uuid,
}
