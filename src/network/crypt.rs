use std::io::Write;

use aes::{
    cipher::{AsyncStreamCipher, NewCipher},
    Aes128,
};
use anyhow::Result;
use cfb8::Cfb8;
use log::trace;
use num_bigint::BigInt;
use rand_chacha::{rand_core::SeedableRng, ChaCha20Rng};
use rand_core::RngCore;
use rsa::{BigUint, PublicKey};
use sha1::{Digest, Sha1};
use tokio::{io::AsyncReadExt, net::tcp::OwnedReadHalf};

use super::models::{datatypes::VarInt, WriteMCBytes};

pub(crate) struct Crypt {
    secret: [u8; 16],
    cipher: Option<Cfb8<Aes128>>,
}

impl Clone for Crypt {
    fn clone(&self) -> Self {
        Self {
            secret: self.secret,
            cipher: None,
        }
    }
}

pub(crate) struct CryptReadStream<'a> {
    pub(crate) crypt: &'a mut Crypt,
    pub(crate) stream: &'a mut OwnedReadHalf,
}

impl CryptReadStream<'_> {
    pub(crate) async fn read_exact(&mut self, buf: &mut [u8]) -> Result<()> {
        self.stream.read_exact(buf).await?;
        self.crypt.decrypt_data(buf);
        Ok(())
    }
}

pub(crate) struct CryptWriteStream<'a> {
    pub(crate) crypt: &'a mut Crypt,
    pub(crate) stream: &'a mut Vec<u8>,
    pub(crate) buf: Vec<u8>,
}

/// # Panic
/// This code will panic if more than VarInt::MAX length data is passed in.
/// A smaller maximum should (and is) be infored externally.
impl Write for CryptWriteStream<'_> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.buf.extend_from_slice(buf);
        Ok(buf.len())
    }

    #[allow(clippy::cast_possible_wrap)]
    #[allow(clippy::cast_possible_truncation)]
    fn flush(&mut self) -> std::io::Result<()> {
        let mut packet_len = Vec::new();
        VarInt::new(self.buf.len() as i32)
            .write_mc_bytes(false, &mut packet_len)
            .unwrap();
        self.crypt.encrypt_data(&mut packet_len);
        self.crypt.encrypt_data(&mut self.buf);
        self.stream.write_all(&packet_len)?;
        self.stream.write_all(&self.buf)?;
        self.stream.flush()?;
        Ok(())
    }
}

impl Crypt {
    /// Generate new [`Crypt`] for encrypting and decrypting.
    /// Starts as disabled, to be enabled at a later time.
    pub(crate) fn new() -> Result<Self> {
        let secret = gen_secret();
        Ok(Crypt {
            secret,
            cipher: None,
        })
    }

    /// Encode the secret key with the given public key.
    /// # Errors
    /// Returns `Err` if the public key is not valid.
    pub(crate) fn enc_secret_with_pub(&self, key: &[u8]) -> Result<Vec<u8>> {
        let (n, e) = rsa_der::public_key_from_der(key)?;
        let pub_key =
            rsa::RsaPublicKey::new(BigUint::from_bytes_be(&n), BigUint::from_bytes_be(&e))?;
        pub_key
            .encrypt(
                &mut ChaCha20Rng::from_entropy(),
                rsa::PaddingScheme::PKCS1v15Encrypt,
                &self.secret,
            )
            .map_err(|e| e.into())
    }

    /// Encode data with the given public key.
    /// # Errors
    /// Returns `Err` if the public key is not valid.
    pub(crate) fn enc_with_pub(val: &[u8], key: &[u8]) -> Result<Vec<u8>> {
        let (n, e) = rsa_der::public_key_from_der(key)?;
        let pub_key =
            rsa::RsaPublicKey::new(BigUint::from_bytes_be(&n), BigUint::from_bytes_be(&e))?;
        pub_key
            .encrypt(
                &mut ChaCha20Rng::from_entropy(),
                rsa::PaddingScheme::PKCS1v15Encrypt,
                val,
            )
            .map_err(|e| e.into())
    }

    /// Computes the Minecraft SHA-1 Hash
    pub(crate) fn compute_server_hash(&self, server_id: &str, pub_key: &[u8]) -> String {
        let mut hasher = Sha1::new();
        hasher.update(server_id.as_bytes());
        hasher.update(&self.secret);
        hasher.update(pub_key);
        hexdigest(hasher.finalize().as_slice())
    }

    /// Enables encryption.
    pub(crate) fn enable(&mut self) {
        self.cipher = Some(Cfb8::<Aes128>::new_from_slices(&self.secret, &self.secret).unwrap());
    }

    /// Decrypts data in place, if enabled. Otherwise, does nothing.
    pub(crate) fn decrypt_data(&mut self, data: &mut [u8]) {
        trace!("Decrypted {} bytes", data.len());
        if self.cipher.is_some() {
            self.cipher.as_mut().unwrap().decrypt(data)
        }
    }

    /// Encrypts data in place, if enabled. Otherwise, does nothing.
    pub(crate) fn encrypt_data(&mut self, data: &mut [u8]) {
        trace!("Encrypted {} bytes", data.len());
        if self.cipher.is_some() {
            self.cipher.as_mut().unwrap().encrypt(data)
        }
    }
}

// Generate random 16 byte key
fn gen_secret() -> [u8; 16] {
    let mut rand = ChaCha20Rng::from_entropy();
    let mut secret = [0u8; 16];
    rand.fill_bytes(&mut secret);
    secret
}

// Non-standard hex digest used by Minecraft.
fn hexdigest(bytes: &[u8]) -> String {
    let bigint = BigInt::from_signed_bytes_be(bytes);
    format!("{:x}", bigint)
}
