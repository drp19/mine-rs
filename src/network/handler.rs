use anyhow::{anyhow, Result};
use log::{info, warn};
use reqwest::{Client, StatusCode};
use tokio::io::AsyncWriteExt;
use tokio::net::tcp::{OwnedReadHalf, OwnedWriteHalf};
use tokio::net::TcpStream;

use crate::network::models::packet::ResponseData;
use crate::network::models::ConnectionStatus;

use super::compress::Compress;
use super::crypt::Crypt;
use super::models::packet::DataPacket;
use super::models::Context;

const VERSION: i32 = 757;

pub struct Connection {
    reader: Reader,
    writer: Writer,
    pub context: Context,
    compression: Compress,
}

struct Reader {
    conn: OwnedReadHalf,
    crypt: Crypt,
}

struct Writer {
    conn: OwnedWriteHalf,
    crypt: Crypt,
}

impl Connection {
    pub async fn new(addr: String, port: u16) -> Result<Self> {
        let stream = TcpStream::connect(format!("{}:{}", addr, port)).await?;
        let (read_half, write_half) = stream.into_split();
        let crypt = Crypt::new()?;
        let reader = Reader {
            conn: read_half,
            crypt: crypt.clone(),
        };
        let writer = Writer {
            conn: write_half,
            crypt,
        };
        Ok(Connection {
            reader,
            writer,
            compression: Compress::new(),
            context: Context {
                status: ConnectionStatus::Handshaking,
            },
        })
    }

    pub fn set_compression(&mut self, threshold: i32) {
        self.compression.set(threshold);
    }

    pub fn set_state(&mut self, status: ConnectionStatus) {
        self.context.status = status;
    }

    pub fn enable_encryption(&mut self) {
        self.reader.crypt.enable();
        self.writer.crypt.enable();
    }

    pub async fn send_packet(&mut self, packet: DataPacket) -> Result<()> {
        self.writer.send_packet(packet, &self.compression).await
    }

    pub async fn read_packet(&mut self) -> Result<DataPacket> {
        self.reader.read(&self.compression, &self.context).await
    }
}

impl Writer {
    async fn send_packet(&mut self, packet: DataPacket, compression: &Compress) -> Result<()> {
        self.conn
            .write(&packet.to_bytes(compression, &mut self.crypt)?)
            .await?;
        Ok(())
    }
}

impl Reader {
    async fn read(&mut self, compression: &Compress, context: &Context) -> Result<DataPacket> {
        DataPacket::from_stream(&mut self.conn, compression, &mut self.crypt, context).await
    }
}

pub async fn login(
    addr: String,
    port: u16,
    username: String,
    access_token: String,
    uuid: String,
) -> Result<Connection> {
    let mut connection = Connection::new(addr.clone(), port).await?;
    let handshake =
        DataPacket::handshake(VERSION.into(), addr.clone(), port, ConnectionStatus::Login);
    connection.set_state(ConnectionStatus::Login);
    let login_start = DataPacket::login_start(username);
    connection.send_packet(handshake).await?;
    connection.send_packet(login_start).await?;

    loop {
        match connection.read_packet().await? {
            DataPacket::Disconnect(c) => {
                warn!("Disconnecting for: {:?}", c);
                break;
            }
            DataPacket::SetCompression(c) => {
                info!("Set compression: {:?}", c);
                connection.set_compression(c.threshold.get_inner());
            }
            DataPacket::LoginSuccess(d) => {
                info!("Logged into server: {:?}", d);
                connection.set_state(ConnectionStatus::Play);
            }
            DataPacket::KeepAlive(k) => {
                info!("Keep alive");
                let response = DataPacket::keep_alive(k.id);
                connection.send_packet(response).await?;
            }
            DataPacket::JoinGame => {
                info!("Join game");
                let client_settings = DataPacket::client_settings(
                    "en_US".to_string(),
                    10,
                    0.into(),
                    true,
                    0x7F,
                    1.into(),
                    false,
                    true,
                );
                let client_status = DataPacket::client_status(0.into());
                connection.send_packet(client_settings).await?;
                connection.send_packet(client_status).await?;
                break;
            }
            DataPacket::EncryptionRequest(e) => {
                info!("Enc request");
                let secret_enc = connection.reader.crypt.enc_secret_with_pub(&e.key)?.clone();
                let verify_enc = Crypt::enc_with_pub(&e.verify_token, &e.key)?;
                let hash = connection
                    .reader
                    .crypt
                    .compute_server_hash(&e.server_id, &e.key);
                let res = Client::new().post("https://sessionserver.mojang.com/session/minecraft/join")
                .body(format!("{{ \"accessToken\": \"{}\", \"selectedProfile\": \"{}\", \"serverId\": \"{}\" }}", access_token, uuid, hash))
                .header("Content-Type", "application/json")
                .send()
                .await?;
                if res.status() == StatusCode::NO_CONTENT {
                    let enc_response = DataPacket::encryption_response(secret_enc, verify_enc);
                    connection.send_packet(enc_response).await?;
                } else {
                    return Err(anyhow!("Failed to authenticate: {:?}", res));
                }
                connection.enable_encryption();
            }
            _ => {}
        }
    }
    Ok(connection)
}

pub async fn status(addr: String, port: u16) -> Result<ResponseData> {
    let mut connection = Connection::new(addr.clone(), port).await?;
    let handshake = DataPacket::handshake(
        (-1_i32).into(),
        addr.clone(),
        port,
        ConnectionStatus::Status,
    );
    let request = DataPacket::request();

    connection.set_state(ConnectionStatus::Status);
    connection.send_packet(handshake).await?;
    connection.send_packet(request).await?;
    let data = connection.read_packet().await?;
    if let DataPacket::Response(response_data) = data {
        Ok(response_data)
    } else {
        Err(anyhow!(
            "Recieved incorrect packet type from server: {:?}",
            data
        ))
    }
}
