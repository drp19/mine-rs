use std::convert::TryFrom;
use std::io::{Cursor, Read, Write};

use anyhow::Result;
use flate2::read::ZlibDecoder;
use flate2::write::ZlibEncoder;
use flate2::Compression;

use crate::network::models::Context;

use super::crypt::{CryptReadStream, CryptWriteStream};
use super::models::datatypes::{VarInt, VarSize};
use super::models::{ReadMCBytes, Size, WriteMCBytes};

pub(crate) struct Compress {
    level: Option<u32>,
}

impl Compress {
    pub(crate) fn new() -> Self {
        Compress { level: None }
    }

    #[allow(clippy::cast_sign_loss)]
    pub(crate) fn set(&mut self, level: i32) {
        if level <= 0 {
            self.level = None
        } else {
            self.level = Some(level as u32)
        }
    }
}

pub(crate) struct CompressWriteStream<'a> {
    pub(crate) compress: &'a Compress,
    pub(crate) stream: &'a mut CryptWriteStream<'a>,
    pub(crate) buf: Vec<u8>,
}

impl Write for CompressWriteStream<'_> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.buf.extend_from_slice(buf);
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        match self.compress.level {
            Some(v) => {
                if self.buf.len() < v as usize {
                    self.stream.write_all(&[0])?;
                    self.stream.write_all(&self.buf)?;
                } else {
                    let mut data_size = Vec::new();
                    VarInt::try_from(VarSize::new(self.buf.len()))
                        .unwrap()
                        .write_mc_bytes(true, &mut data_size)
                        .unwrap();
                    self.stream.write_all(&data_size)?;
                    ZlibEncoder::new(&mut self.stream, Compression::default())
                        .write_all(&self.buf)?;
                }
            }
            _ => {
                self.stream.write_all(&self.buf)?;
            }
        }
        self.stream.flush()
    }
}
pub(crate) struct CompressReadStream<'a> {
    pub(crate) compress: &'a Compress,
    pub(crate) stream: &'a mut CryptReadStream<'a>,
    pub(crate) context: &'a Context,
}

impl CompressReadStream<'_> {
    pub(crate) async fn read_bytes(&mut self, len: usize) -> Result<Vec<u8>> {
        let mut maybe_compressed = vec![0; len];
        self.stream.read_exact(&mut maybe_compressed).await?;
        let mut maybe_compressed_reader = Cursor::new(maybe_compressed);
        let mut output;
        if self.compress.level.is_some() {
            let data_size =
                VarSize::read_mc_bytes(&mut maybe_compressed_reader, self.context, None)?;
            if data_size.get_inner() == 0 {
                output = vec![0; len - data_size.size(false)];
            } else {
                output = vec![0; data_size.get_inner() as usize];
                ZlibDecoder::new(maybe_compressed_reader).read_exact(&mut output)?;
                return Ok(output);
            }
        } else {
            output = vec![0; len];
        }
        maybe_compressed_reader.read_exact(&mut output)?;
        Ok(output)
    }
}
