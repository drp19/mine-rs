#[cfg(test)]
mod tests {
    use std::{fs::File, io::BufReader};

    use serde::Deserialize;

    use crate::network::models::ConnectionStatus;

    use super::super::network::handler::{login, status};

    #[tokio::test]
    async fn test_status() {
        let reader = BufReader::new(File::open("test_config.json").unwrap());
        let config: Config = serde_json::from_reader(reader).unwrap();

        let status = status(config.address, config.port).await;
        println!("{:?}", status);
    }

    #[tokio::test]
    async fn test_online() {
        let reader = BufReader::new(File::open("test_config.json").unwrap());
        let config: Config = serde_json::from_reader(reader).unwrap();

        let result = login(
            config.address,
            config.port,
            config.username,
            config.access_token,
            config.uuid,
        )
        .await;
        match result {
            Ok(c) => {
                assert_eq!(c.context.status, ConnectionStatus::Play);
            }
            Err(e) => {
                println!("{:?}", e);
                panic!("Failed")
            }
        }
    }

    #[derive(Deserialize)]
    struct Config {
        address: String,
        port: u16,
        username: String,
        access_token: String,
        uuid: String,
    }
}
