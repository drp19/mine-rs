extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;

use proc_macro::TokenStream;
use syn::{
    parse_macro_input,
    DeriveInput,
};

/// Writing structs:
/// - For each field, convert to a given type if necessary.
/// - If param says to, prefix the size.
/// - Convert each field to bytes
/// 
/// Writing enums:
/// - Create match expression such that for each variant, do the following:
///     - Unit: do nothing
///     - Single item: parse item into bytes 
/// 
/// Struct/enum attributes:
/// - `json`: convert to string and use serde_json to encode
///     - Field level attributes are ignored
/// - `encode_as`: convert whole struct to given type and use `write_mc_bytes` from that type
/// 
/// Struct field attributes:
/// - `no_prefix`: if provided, pass `true` to the no_prefix parameter for this field
/// - `encode_as`: convert field to given type and use `write_mc_bytes` from that type
/// - 'no_encode': ignore this field
/// 
#[proc_macro_derive(WriteMCBytes, attributes(no_prefix, prefix_with, json, encode_as, no_encode))]
pub fn write_mc_bytes(input: TokenStream) -> TokenStream {
    // Parse the string representation
    let ast = parse_macro_input!(input as DeriveInput);

    // Check attributes
    let name = &ast.ident;
    let (is_json, no_prefix, prefix_as, encode_as) = ast.attrs.iter().fold((false, false, None, None), |(aj, ap, apr, ae), v|
            if v.path.is_ident("json") {
                (true, ap, apr, ae)
            } else if v.path.is_ident("no_prefix") {
                (aj, true, apr, ae)
            } else if v.path.is_ident("prefix_with") {
                let tokens = &v.tokens;
                (aj, ap, Some(quote!(#tokens)), ae)
            } else if v.path.is_ident("encode_as") {
                if let Ok(t) = &mut v.parse_args::<syn::Type>() {
                    if let syn::Type::Path(syn::TypePath { qself: _, path }) = t {
                        fix_angle_bracket(path);
                        (aj, ap, apr, Some(path.clone()))
                    } else {
                        panic!("Need datatype")
                    }
                } else {
                    panic!("Need datatype")
                }
            } else {
                (aj, ap, apr, ae)
            });
    // gen inner
    let inner = if is_json {
        quote! {
            serde_json::to_string(self).unwrap().write_mc_bytes(false, bytes)?;
            Ok(())
        }
    } else if encode_as.is_some() {
        let encode_type = encode_as.unwrap();
        quote! {
            #encode_type::from(self).write_mc_bytes(no_size_prefix, bytes)?;
            Ok(())
        }
    } else {
        match &ast.data {
            syn::Data::Struct(struct_data) => write_handle_struct(struct_data, no_prefix),
            syn::Data::Enum(enum_data) => write_handle_enum(enum_data, name, prefix_as),
            syn::Data::Union(_) => panic!("Cannot add macro to union."),
        }
    };
    write_gen_tokens(name, inner)
}

fn write_handle_enum(data: &syn::DataEnum, name: &syn::Ident, prefix_as: Option<quote::__private::TokenStream>) -> quote::__private::TokenStream {
    let mut enum_inner = quote! ();
    data.variants.iter().for_each(|v| {
        let variant_name = &v.ident;
        let id = v.attrs.iter().fold(quote!(None), |ai, v| {
            if v.path.is_ident("id") {
                let tokens = &v.tokens;
                quote!(#tokens )
            } else {
                ai
            }
        });
        let prefix = match &prefix_as {
            Some(p) => quote! {
                if !no_size_prefix {
                    #p(#id).write_mc_bytes(false, bytes)
                } else {
                    Ok(())
                }
            },
            None => quote! {
                if !no_size_prefix {
                    VarInt::new(#id).write_mc_bytes(false, bytes)
                } else {
                    Ok(())
                }
            },
        };
        if let syn::Fields::Unnamed(_) = &v.fields {
            enum_inner.extend(quote! {
                #name::#variant_name (f, ..) => {
                    #prefix?;
                    f.write_mc_bytes(no_size_prefix, bytes)},
            });
        } else if let syn::Fields::Unit = v.fields {
            enum_inner.extend(quote! {
                #name::#variant_name => #prefix,
            });
        } else {
            panic!("Cannot process named fields on enum")
        };
    });

    quote! {
        match self {
            #enum_inner
        }
    }
}

fn write_handle_struct(data: &syn::DataStruct, no_prefix: bool) -> quote::__private::TokenStream {
    let mut field_inner = quote!();
    match &data.fields {
        syn::Fields::Named(fields) => {
            fields.named.iter().map(|f| f).for_each(|f| {
                let ident = f.ident.as_ref().unwrap();
                let (mut no_prefix, ignore, prefix_with, encode_as) = f.attrs.iter().fold((false, false, None, None), |(ap, ai,apr, ae), v| {
                    if v.path.is_ident("no_prefix") {
                        (true, ai, apr, ae)
                    } else if v.path.is_ident("no_encode") {
                        (ap, true, apr, ae)
                    } else if v.path.is_ident("prefix_with") {
                        let tokens = &v.tokens;
                        (ap, ai, Some(quote!(#tokens)), ae)
                    } else if v.path.is_ident("encode_as") {
                        if let Ok(t) = &mut v.parse_args::<syn::Type>() {
                            if let syn::Type::Path(syn::TypePath { qself: _, path }) = t {
                                fix_angle_bracket(path);
                                (ap, ai, apr, Some(path.clone()))
                            } else {
                                panic!("Need datatype")
                            }
                        } else {
                            panic!("Need datatype")
                        }
                    } else {
                        (ap, ai, apr, ae)
                    }
                });
                if prefix_with.is_some() {
                    no_prefix = true;
                    let prefix = prefix_with.unwrap();
                    field_inner.extend(quote! {
                        (self.#ident.size(false) as #prefix).write_mc_bytes(false, bytes)?;
                    })
                }
                if !ignore {
                    field_inner.extend(match encode_as {
                        Some(t) => quote! {
                            #t::from(self.#ident).write_mc_bytes(#no_prefix, bytes)?;
                        },
                        None => quote! {
                            self.#ident.write_mc_bytes(#no_prefix, bytes)?;
                        },
                    });
                }
            })
        }
        syn::Fields::Unnamed(_) | syn::Fields::Unit => panic!("Need named fields."),
    }
    if no_prefix { 
        quote! {
            #field_inner
            Ok(())
        }  
    } else {
        quote! {
            if !no_size_prefix {
                VarInt::new(self.size(true) as i32).write_mc_bytes(false, bytes)?;
            }
            #field_inner
            Ok(())
        }
    }
}

fn write_gen_tokens(name: &syn::Ident, inner: quote::__private::TokenStream) -> TokenStream {
    quote! {
        impl WriteMCBytes for #name {
            #[allow(unused_parens)]
            fn write_mc_bytes<W: Write>(&self, no_size_prefix: bool, bytes: &mut W) -> Result<()> {
                #inner
            }
        }
    }.into()
}

/// Size of structs:
/// - For each field, convert to a given type if necessary.
/// - If param says to, include the size of the size prefix.
/// - Sum the sizes
/// 
/// Size of enums:
/// - Create match expression such that for each variant, do the following:
///     - Unit: size 0
///     - Single item: get size
/// 
/// Struct/enum attributes:
/// - `json`: convert to string and use serde_json to get the size
///     - Field level attributes are ignored
/// - `encode_as`: convert whole struct to given type and use `size` from that type
/// 
/// Struct field attributes:
/// - `no_prefix`: if provided, pass `false` to the do_prefix parameter for this field
/// - `encode_as`: convert field to given type and use `size` from that type
/// - 'no_encode': ignore this field for size
/// 
#[proc_macro_derive(Size, attributes(no_prefix, json, encode_as, no_encode))]
pub fn size(input: TokenStream) -> TokenStream {
    // Parse the string representation
    let ast = parse_macro_input!(input as DeriveInput);

    // Check attributes
    let name = &ast.ident;
    let (is_json, encode_as) = ast.attrs.iter().fold((false, None), |(aj, ae), v|
            if v.path.is_ident("json") {
                (true, ae)
            } else if v.path.is_ident("encode_as") {
                if let Ok(t) = &mut v.parse_args::<syn::Type>() {
                    if let syn::Type::Path(syn::TypePath { qself: _, path }) = t {
                        fix_angle_bracket(path);
                        (aj, Some(path.clone()))
                    } else {
                        panic!("Need datatype")
                    }
                } else {
                    panic!("Need datatype")
                }
            } else {
                (aj, ae)
            });
    let inner = if is_json {
        quote! {
            serde_json::to_string(self).unwrap().size(do_prefix_size)
        }
    } else if encode_as.is_some() {
        let encode_attr = encode_as.unwrap();
        quote! {
            #encode_attr::from(self).size(do_prefix_size)
        }
    } else {
        let mut size = quote! {
            let mut size = 0
        };
        size.extend(match &ast.data {
            syn::Data::Struct(struct_data) => size_handle_struct(struct_data),
            syn::Data::Enum(enum_data) => size_handle_enum(enum_data, name),
            syn::Data::Union(_) => panic!("Cannot add macro to union."),
        });
        size.extend(quote!{
            ; 
            size
        });
        size
    };
    size_gen_tokens(name, inner)
}

fn size_handle_enum(data: &syn::DataEnum, name: &syn::Ident) -> quote::__private::TokenStream {
    let mut enum_inner = quote! ();
    data.variants.iter().for_each(|v| {
        let enum_name = &v.ident;
        if let syn::Fields::Unnamed(_) = &v.fields {
            enum_inner.extend(quote! {
                #name::#enum_name (f,..) => f.size(do_prefix_size),
            });
        } else if let syn::Fields::Unit = v.fields {
            enum_inner.extend(quote! {
                #name::#enum_name => 0,
            });
        } else {
            panic!("Named fields on enum")
        };
    });
    quote! {
        + match self {
            #enum_inner
        }
    }
}

fn size_handle_struct(data: &syn::DataStruct) -> quote::__private::TokenStream {
    let mut field_inner = quote!();
    match &data.fields {
        syn::Fields::Named(fields) => {
            fields.named.iter().map(|f| f).for_each(|f| {
                let ident = f.ident.as_ref().unwrap();
                let (no_prefix, ignore, encode_as) = f.attrs.iter().fold((false, false, None), |(ap, ai,ae), v| {
                    if v.path.is_ident("no_prefix") {
                        (true, ai, ae)
                    } else if v.path.is_ident("no_encode") {
                        (ap, true, ae)
                    } else if v.path.is_ident("encode_as") {
                        if let Ok(t) = &mut v.parse_args::<syn::Type>() {
                            if let syn::Type::Path(syn::TypePath { qself: _, path }) = t {
                                fix_angle_bracket(path);
                                (ap, ai, Some(path.clone()))
                            } else {
                                panic!("Need datatype")
                            }
                        } else {
                            panic!("Need datatype")
                        }
                    } else {
                        (ap, ai, ae)
                    }
                });
                if !ignore {
                    field_inner.extend(match encode_as {
                        Some(t) => quote! {
                            + #t::from(self.#ident).size(!#no_prefix)
                        },
                        None => quote! {
                            + self.#ident.size(!#no_prefix)
                        },
                    });
                }
            })
        }
        syn::Fields::Unnamed(_) | syn::Fields::Unit => panic!("Need named fields."),
    }
    field_inner
}

fn size_gen_tokens(name: &syn::Ident, inner: quote::__private::TokenStream) -> TokenStream {
    quote! {
        impl Size for #name {
            fn size(&self, do_prefix_size: bool) -> usize {
                #inner
            }
        }
    }.into()
}


/// Reading structs:
/// - For each field, read to a given type if necessary.
/// - Check if size provided, create fields from bytes
/// - Instantiate struct or call new
/// 
/// Reading enums:
/// - TODO
/// 
/// Struct/enum attributes:
/// - `require_size` : 
///     - if "prefix", ignore provided size and read size as prefix
///     - if "param", return `Err` if there is no param provided size
/// - `call_new` : call the new() method with all requireed parameters rather than instantiating the struct
/// - `json`: read to string and use serde_json to encode
///     - Field level attributes are ignored
/// - `encode_as`: use `read_mc_bytes` on the given type and try to convert struct from that type
/// 
/// Struct field attributes:
/// - `size`: use the provided expression and pass Some(expr) to the size parameter when converting the field
/// - `encode_as`: use `read_mc_bytes` on the given type and try to convert field from that type
/// - 'no_encode': ignore this field - requires `call_new`
/// 
#[proc_macro_derive(ReadMCBytes, attributes(size, no_prefix, require_size, encode_as, json, call_new, no_encode, id))]
pub fn read_mc_bytes(input: TokenStream) -> TokenStream {
    // Parse the string representation
    let ast = parse_macro_input!(input as DeriveInput);

    // Check attributes
    let name = &ast.ident;
    let (is_json, call_new, require_size, id, encode_as) = ast.attrs.iter().fold((false, false, None, quote!(None), None), |(aj, an, ar, ai, ae), v|
            if v.path.is_ident("json") {
                (true, an, ar, ai, ae)
            } else if v.path.is_ident("call_new") {
                (aj, true, ar, ai, ae)
            } else if v.path.is_ident("require_size") {
                if let Ok(syn::Meta::NameValue(p)) = v.parse_meta() {
                    let size_tokens = if let syn::Lit::Str(s) = p.lit {
                        if s.value().eq("prefix") {
                            quote!(let size = Some(VarInt::read_mc_bytes(bytes, context, None)?.get_inner_usize()))
                        } else if s.value().eq("param") {
                            quote!(if size.is_none() {return Err(anyhow!("Size must be provided."))})
                        } else {
                            panic!("Unknown string in require_size")
                        }
                    } else {
                        panic!("Need string in require_size")
                    };
                    (aj, an, Some(size_tokens), ai, ae)
                } else {
                    panic!("Need string in require_size")
                }
            } else if v.path.is_ident("id") {
                let tokens = &v.tokens;
                (aj, an, ar, quote!(#tokens ), ae)
            } else if v.path.is_ident("encode_as") {
                if let Ok(t) = &mut v.parse_args::<syn::Type>() {
                    if let syn::Type::Path(syn::TypePath { qself: _, path }) = t {
                        fix_angle_bracket(path);
                        (aj, an, ar, ai, Some(path.clone()))
                    } else {
                        panic!("Need datatype")
                    }
                } else {
                    panic!("Need datatype")
                }
            } else {
                (aj, an, ar, ai, ae)
            });
    
    let mut inner = quote!(#require_size);
    inner.extend(if is_json {
        quote! {
            let json = String::read_mc_bytes(bytes, context, None)?;
            Ok(serde_json::from_str(&json)?)
        }
    } else if encode_as.is_some() {
        let encode_attr = encode_as.unwrap();
        quote! {
            #encode_attr::read_mc_bytes(bytes, context, size)?.try_into()
        }
    } else {
        match &ast.data {
            syn::Data::Struct(struct_data) => read_handle_struct(struct_data, name, call_new),
            syn::Data::Enum(enum_data) => read_handle_enum(enum_data, name, id), 
            syn::Data::Union(_) => panic!("Cannot add macro to union."),
        }
    });
    read_gen_tokens(name, inner)
}

fn fix_angle_bracket(path: &mut syn::Path) {
    path.segments.iter_mut().for_each(|s| {
        if let syn::PathArguments::AngleBracketed(a) = &mut s.arguments {
            a.colon2_token = Some(syn::token::Colon2::default());
        }
    })
}

fn read_handle_enum(data: &syn::DataEnum, name: &syn::Ident, struct_id: quote::__private::TokenStream) -> quote::__private::TokenStream {
    let mut enum_inner = quote! ();
    data.variants.iter().for_each(|v| {
        let enum_name = &v.ident;
        let (id, size) = v.attrs.iter().fold((None, quote!(size)), |(ai, asi), v|
            if v.path.is_ident("id") {
                let tokens = &v.tokens;
                (Some(quote!(#tokens)), asi)
            } else if v.path.is_ident("size") {
                let tokens = &v.tokens;
                (ai, quote!(Some #tokens))
            } else {
                (ai, asi)
            });
        if id.is_none() {
            panic!("Need ID field for reading enum")
        }
        let id = id.unwrap();
        if let syn::Fields::Unnamed(f) = &v.fields {
            let field = &f.unnamed.first().unwrap().ty;
            enum_inner.extend(quote! {
                x if x == #id => Ok(#name::#enum_name(#field::read_mc_bytes(bytes, context, #size)?)),
            });
        } else if let syn::Fields::Unit = v.fields {
            enum_inner.extend(quote! {
                x if x == #id => Ok(#name::#enum_name),
            });
        } else {
            panic!("Named fields on enum")
        };
    });
    quote! {
        let id = match #struct_id {
            Some(v) => v,
            None => i8::read_mc_bytes(bytes, context, None)? as i32
        };
        match id {
            #enum_inner
            _ => Ok(#name::Unknown)
        }
    }
}

fn read_handle_struct(data: &syn::DataStruct, name: &syn::Ident, call_new: bool) -> quote::__private::TokenStream {
    let mut field_inner = quote!();
    let mut field_list: syn::punctuated::Punctuated<syn::Ident, syn::Token![,]> =
        syn::punctuated::Punctuated::new();
    match &data.fields {
        syn::Fields::Named(fields) => {
            fields.named.iter().map(|f| f).for_each(|f| {
                let ident = f.ident.as_ref().unwrap();
                let mut datatype = f.ty.clone();
                if let syn::Type::Path(syn::TypePath { qself: _, path }) = &mut datatype {
                    fix_angle_bracket(path);
                }
                let (size, ignore, encode_as) = f.attrs.iter().fold((quote!(None), false, None), |(asi, ai,ae), v| {
                    if v.path.is_ident("size") {
                        let tokens = &v.tokens;
                        (quote!(Some #tokens), ai, ae)
                    } else if v.path.is_ident("no_encode") {
                        (asi, true, ae)
                    } else if v.path.is_ident("encode_as") {
                        if let Ok(t) = &mut v.parse_args::<syn::Type>() {
                            if let syn::Type::Path(syn::TypePath { qself: _, path }) = t {
                                fix_angle_bracket(path);
                                (asi, ai, Some(path.clone()))
                            } else {
                                panic!("Need datatype")
                            }
                        } else {
                            panic!("Need datatype")
                        }
                    } else {
                        (asi, ai, ae)
                    }
                });
                if ignore && !call_new {
                    panic!("Cannot use ignore to read without new");
                }
                if !ignore {
                    field_inner.extend(quote! {
                        let field_size = #size;
                    });
                    field_inner.extend(match encode_as {
                        Some(t) => 
                        quote! {
                            let #ident = #t::read_mc_bytes(bytes, context, field_size)?.try_into()?;
                        },
                        None =>
                        quote! {
                            let #ident = #datatype::read_mc_bytes(bytes, context, field_size)?;
                        }
                    });
                    field_list.push(ident.clone());
                };
            })
        }
        syn::Fields::Unnamed(_) | syn::Fields::Unit => panic!("Need named fields."),
    };
    if call_new {
        quote!{
            #field_inner
            Ok(#name::new(#field_list)?)
        }
    } else {
        quote! {
            #field_inner
            Ok(#name {
                #field_list
            })
        }
    }
}

fn read_gen_tokens(name: &syn::Ident, inner: quote::__private::TokenStream) -> TokenStream {
    quote! {
        impl ReadMCBytes for #name {
            #[allow(unused_parens)]
            fn read_mc_bytes<R:Read>(bytes: &mut R, context: &Context, size: Option<usize>) -> Result<Self> {
                #inner
            }
        }
    }.into()
}
